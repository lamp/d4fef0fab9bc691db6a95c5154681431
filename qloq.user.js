// ==UserScript==
// @name         Clock in Discord Status
// @namespace    lamp@qonq.gq
// @version      1
// @author       Lamp
// @match        https://discord.com/*
// @grant        none
// @run-at       document-start
// ==/UserScript==

var TOKEN = JSON.parse(localStorage.token);

var qloqqs = new Map([
	["12:0",  '🕛'],
	["12:30", '🕧'],
	["1:0",   '🕐'],
	["1:30",  '🕜'],
	["2:0",   '🕑'],
	["2:30",  '🕝'],
	["3:0",   '🕒'],
	["3:30",  '🕞'],
	["4:0",   '🕓'],
	["4:30",  '🕟'],
	["5:0",   '🕔'],
	["5:30",  '🕠'],
	["6:0",   '🕕'],
	["6:30",  '🕡'],
	["7:0",   '🕖'],
	["7:30",  '🕢'],
	["8:0",   '🕗'],
	["8:30",  '🕣'],
	["9:0",   '🕘'],
	["9:30",  '🕤'],
	["10:0",  '🕙'],
	["10:30", '🕥'],
	["11:0",  '🕚'],
	["11:30", '🕦']
]);

function qloq(hours, minutes) {
	if (minutes < 15) {
		minutes = 0;
	}
	else if (minutes >= 15 && minutes < 45) {
		minutes = 30;
	} else if (minutes >= 45) {
		minutes = 0;
		hours += 1;
		if (hours > 12) hours = 1;
	}
	return qloqqs.get(`${hours}:${minutes}`);
}

function setStatus(emoji_name, text) {
	fetch("https://discord.com/api/v8/users/@me/settings", {
		"headers": {
			"authorization": TOKEN,
			"content-type": "application/json",
		},
		"body": JSON.stringify({ custom_status: {text, emoji_name} }),
		"method": "PATCH"
	});
}

(function toq() {
	var d = new Date();
	var timeStringNow = d.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
	var g = timeStringNow.substr(0,timeStringNow.indexOf(' ')).split(':');
	setStatus(qloq(Number(g[0]), Number(g[1])), timeStringNow);
	d.setMinutes(d.getMinutes() + 1);
	d.setSeconds(0);
	setTimeout(toq, d - Date.now());
})();
